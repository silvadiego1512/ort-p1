$("#btnCalcularOperacion").click(operate);


function operate() {
	var value1;
	var value2;
	var value3;
	var sumResult
	var value1sq;
	var result;
	
	value1 = Number($("#txtValue1").val());
	value2 = Number($("#txtValue2").val());
	value3 = Number($("#txtValue3").val());
	
	value1sq = value1 * value1;
	sumResult = value2 + value3;
	result = value1sq - sumResult;
	
	
	$("#oprResult").html("El resultado de la resta de " + value1sq + " (" + value1 + " al cuadrado) y " + sumResult + " (" + value2 + " + " + value3 + ") es " + result);
}


