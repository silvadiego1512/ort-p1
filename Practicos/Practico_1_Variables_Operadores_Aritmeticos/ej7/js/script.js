$("#btnCalcularResto").click(remainderValues);


function remainderValues() {
	var value1;
	var value2;
	var result;
	
	value1 = Number($("#txtValue1").val());
	value2 = Number($("#txtValue2").val());
	
	result = value1 % value2;
	
	$("#remainderResult").html("El resto de dividir " + value1 + " entre " + value2 + " es " + result);
}


