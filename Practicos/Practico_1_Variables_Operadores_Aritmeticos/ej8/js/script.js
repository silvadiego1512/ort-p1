$("#btnCalcularOperacion").click(operate);


function operate() {
	var value1;
	var value2;
	var value3;
	var sumResult;
	var result;
	
	value1 = Number($("#txtValue1").val());
	value2 = Number($("#txtValue2").val());
	value3 = Number($("#txtValue3").val());
	
	sumResult = value1 + value2;
	result = sumResult - value3;
	
	
	$("#oprResult").html("El resultado de la suma de " + value1 + " + " + value2 + " es " + sumResult + ", menos " + value3 + " es " + result);
}


