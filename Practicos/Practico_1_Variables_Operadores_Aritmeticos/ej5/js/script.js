$("#btnCalcularArea").click(calculateArea);


function calculateArea() {
	var valueSide;
	var surface;
	
	valueSide = Number($("#txtSide").val());
	
	surface = valueSide * valueSide;
	
	$("#area").html("El area del cuadrado de " + valueSide + " de lado es " + surface);
}


