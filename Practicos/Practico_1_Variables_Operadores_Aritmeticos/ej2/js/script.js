$("#btnCalcularSuma").click(sumValues);


function sumValues() {
	var value1;
	var value2;
	var result;
	
	value1 = Number($("#txtValue1").val());
	value2 = Number($("#txtValue2").val());
	
	result = value1 + value2;
	
	$("#sumResult").html("El resultado de la suma de " + value1 + " + " + value2 + " es " + result);
}


