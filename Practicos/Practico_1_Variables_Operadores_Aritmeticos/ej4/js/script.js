$("#btnOperate").click(operateValues);


function operateValues() {
	var value1;
	var value2;
	var sumResult;
	var byResult;
	
	value1 = Number($("#txtValue1").val());
	value2 = Number($("#txtValue2").val());
	
	sumResult = value1 + value2;
	
	byResult = value1 * value2;
	
	$("#sum").html("El resultado de la suma de " + value1 + " + " + value2 + " es " + sumResult);
	$("#by").html("El resultado de la multiplicaci&oacute;n de " + value1 + " x " + value2 + " es " + byResult);
}


