$("#btnAddNumber").click(addNumber);
$("#btnShowTotal").click(showTotal);

var numberAdd;
var total = 0;

function addNumber() {
	numberAdd = Number($("#txtNumberAdd").val());
	total = total + numberAdd;
}

function showTotal() {
	$("#total").html("El total sumado es " + total);
}