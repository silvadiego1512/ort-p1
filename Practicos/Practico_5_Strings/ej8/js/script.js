$("#btnIngresar").click(mostrar);

function getTimesOfLetter(_string, _char) {
    return Number(_string.split(_char).length - 1);
}

function getWords(_string) {
    if (_string == "") {
        return 0;
    } else {
        return getTimesOfLetter(_string, " ") + 1;
    }
}

function mostrar() {
    var string = $("#txtParameter1").val();
    $("#pTexto").html(getWords(string));
}


