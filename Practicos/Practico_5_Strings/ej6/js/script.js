$("#btnIngresar").click(mostrar);

function correctCase(_string) {
    var _newString = _string.toLowerCase();
    var _firstLetter = _newString.charAt(0);
    return _firstLetter.toUpperCase() + _newString.substring(1, _newString.length);
}

function mostrar() {
    var string = $("#txtParameter1").val();
    $("#pTexto").html(correctCase(string));
}


