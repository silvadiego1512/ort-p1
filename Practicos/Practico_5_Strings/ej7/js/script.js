$("#btnIngresar").click(mostrar);

function checkSubstring(_string, _substring) {
    
    if (_string.indexOf(_substring) == -1) {
        return "No es subcadena";
    } else {
        return "Es subcadena en posicion " + _string.indexOf(_substring);
    }
}


function mostrar() {
    
    var string = $("#txtParameter1").val();
    var substring = $("#txtParameter2").val();
    
    
    $("#pTexto").html(checkSubstring(string, substring));
}


