$("#btnIngresar").click(mostrar);

function getTimesOfLetter(_string, _char) {
    return Number(_string.split(_char).length - 1);
}

function getVowels(_string) {
    return getTimesOfLetter(_string, "A") + getTimesOfLetter(_string, "E") + getTimesOfLetter(_string, "I") + getTimesOfLetter(_string, "O") + getTimesOfLetter(_string, "U") + getTimesOfLetter(_string, "a") + getTimesOfLetter(_string, "e") + getTimesOfLetter(_string, "i") + getTimesOfLetter(_string, "o") + getTimesOfLetter(_string, "u")
}

function mostrar() {
    var string = $("#txtParameter1").val();
    $("#pTexto").html(getVowels(string));
}


