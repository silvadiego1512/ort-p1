$("#btnSubmit").click(accion);


function accion() {
    
    // Determina Variables
    var numero1 = Number($("#txtValue1").val());
    var numero2 = Number($("#txtValue2").val());
    var response;
    
    // Determinar si la condicion se cumple
    if ( numero1 > numero2 ) {
        response = numero1 - numero2;
    } else {
        response = numero2 - numero1;
    }

    // Imprime variable $response en HTML
    $("#pResponse").html(response);
    
}