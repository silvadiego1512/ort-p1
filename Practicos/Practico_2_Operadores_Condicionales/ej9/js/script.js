$("#btnSubmit").click(accion);

var br = "<br />";

function accion() {
    
    // Determina Variables
    var temperatura = Number($("#txtTemp").val());
    var dia = $("#txtDia").val();
    var statusRopa;
    
    // Chequear Estado Ropa
    
    // Chequea que temperatura sea numero
    if (isNaN(temperatura)) {
        // Si temperatura no es numero define null en variable, chequeado sobre el final
        statusRopa = null;
    } else {
        // Si es un numero, prosigue normalmente
        if ( temperatura < 10 ) {
            statusRopa = "Abrigarse mucho!";
        } else {
            if ( temperatura > 20 ) {
                statusRopa = "Ponerse ropa comoda";
            } else {
                statusRopa = "Abrigo moderado";
            };
        };
    };
    
    // Chequear Estado Dia de la Semana
    var statusDia;
    var diasTrabaja = [
        "lunes",
        "martes",
        "miercoles",
        "jueves",
        "viernes",
        "sabado",
    ];
    
    if ( diasTrabaja.indexOf(dia) != -1 ) {
        
        statusDia = "Ir al trabajo";
        
    } else {
        
        if ( dia === "domingo") {
            
            statusDia = "No ir al trabajo";
            
        } else {
            
            // Si no se ingreso un dia, define null en variable, condicion chequeada antes de mostrar resultado
            statusDia = null;
            
        };
    };
    
    // Chequea que ninguna de las condiciones anteriores hayan saltado un valor como mal ingresado (variable seteada a null)
    if (statusDia === null || statusRopa === null) {
        
        // Si hay una null, rezonga al usuario
        $("#pResponse").html("Ingrese los valores correctamente");
        
    } else {
        
        // Si los valores fueron ingresados correctamente, muestra el resultado
        $("#pResponse").html("Levantarse" + br);
        $("#pResponse").append(statusRopa + br);
        $("#pResponse").append(statusDia + br);
        
    };
    
};