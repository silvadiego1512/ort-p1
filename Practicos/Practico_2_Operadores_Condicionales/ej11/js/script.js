$("#btnSubmit").click(accion);
$("#btnEnd").click(finalizar);

var numSueldos = 0;
var totalSueldos = 0;
var minSueldo = 9999999999;

var br = "<br />";

function accion() {
    var sueldo = Number($("#txtValue").val());
    
    // Chequea si es menor que el minimo
    if ( sueldo < minSueldo ) {
        minSueldo = sueldo;
    }
    
    // Agrega al total
    numSueldos = numSueldos + 1;
    
    // Agrega a la suma
    totalSueldos = totalSueldos + sueldo;
}

function finalizar() {
    // Finalizar e imprimir todo
    
    $("#h3ResponseTitle").html("Estadisticas");
    $("#pResponse").html("<b>Sueldo minimo:</b> " + minSueldo + br);
    $("#pResponse").append("<b>Empleados a Pagar:</b> " + numSueldos + br);
    $("#pResponse").append("<b>Total a Pagar:</b> " + totalSueldos + br);
    
}