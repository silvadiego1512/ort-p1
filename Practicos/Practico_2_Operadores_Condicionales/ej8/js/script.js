$("#btnSubmit").click(accion);


function accion() {
    
    // Determina Variables
    var numero = Number($("#txtValue").val());
    var response;
    
    // Determinar si la condicion se cumple
    if ( numero > 30 ) {
        response = "El numero es mayor que 30";
    } else {
        if ( numero < 10 ) {
            response = "El numero es menor que 10";
        } else {
            response = "El numero esta entre 10 y 30"
        }
    }

    // Imprime variable $response en HTML
    $("#pResponse").html(response);
    
}