$("#btnSubmit").click(accion);


function accion() {
    
    // Determina Variables
    var numero1 = Number($("#txtValue1").val());
    var numero2 = Number($("#txtValue2").val());
    var operacion = $("#txtOperacion").val();
    var response;
    
    // Determinar si la condicion se cumple
    if ( operacion === "S" ) {
        response = numero1 + numero2;
    } else {
        if ( operacion === "R" ) {
            response = numero1 - numero2;
        } else {
            if ( operacion === "M" ) {
                response = numero1 * numero2;
            } else {
                if ( operacion === "D" ) {
                    response = numero1 / numero2;
                } else {
                    response = "Ingresa una operacion correcta"
                }
            }
        }
    }

    // Imprime variable $response en HTML
    $("#pResponse").html(response);
    
}