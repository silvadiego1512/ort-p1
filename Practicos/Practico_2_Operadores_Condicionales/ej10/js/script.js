$("#btnSubmit").click(accion);
$("#btnEnd").click(finalizar);

var numPerdieron = 0;
var numSalvaron = 0;
var numMuyBueno = 0;
var marks = 0;
var markSum = 0;
var markMax = 0;
var markMin = 100;

var br = "<br />";

// TIP: Cada presion de "Agregar" imprime datos a la consola para verificar el correcto funcionamiento 

function accion() {
    var mark = Number($("#txtValue").val());
    
    
    // Chequea si perdieron, <70, agrega a la nota
    if ( mark < 70 ) {
        console.log("Perdio");                                     // DEBUG
        numPerdieron = numPerdieron + 1;
    }
    
    // Chequea si salvaron, >70, agrega a la nota
    if ( mark >= 70 ) {
        console.log("Salvo");                                      // DEBUG
        numSalvaron = numSalvaron + 1;
    }
    
    // Chequea si fue muy bueno, >90, agrega a la nota
    if ( mark > 90 ) {
        console.log("Muy Bueno");                                   // DEBUG
        numMuyBueno = numMuyBueno + 1;
    }
    
    // Chequea si es menor que la minima
    if ( mark < markMin ) {
        console.log("Nuevo Minimo: " + mark);                                   // DEBUG
        markMin = mark;
    }
    
    // Chequea si es mayor que la maxima
    if ( mark > markMax ) {
        console.log("Nuevo Maximo: " + mark);                                   // DEBUG
        markMax = mark;
    }
    
    // Agrega al total
    marks = marks + 1;
    
    // Agrega a la suma para el promedio
    markSum = markSum + mark;
    
    console.log("----------------------");                                   // DEBUG
}

function finalizar() {
    // Finalizar e imprimir todo
    
    $("#h3ResponseTitle").html("Estadisticas");
    
    var markAvg = markSum / marks;
    
    $("#pResponse").html("<b>Alumnos que Perdieron:</b> " + numPerdieron + br);
    $("#pResponse").append("<b>Alumnos que Salvaron:</b> " + numSalvaron + " (" + numMuyBueno + " de esos fueron muy buenos)" + br);
    $("#pResponse").append("<b>Promedio de Notas:</b> " + Math.round(markAvg) + br);
    $("#pResponse").append("<b>Nota maxima:</b> " + markMax + br);
    $("#pResponse").append("<b>Nota minima:</b> " + markMin + br);
    
}