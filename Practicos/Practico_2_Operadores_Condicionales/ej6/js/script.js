$("#btnSubmit").click(accion);


function accion() {
    
    // Determina Variables
    var numero = Number($("#txtValue").val());
    var response;
    
    // Determinar si la condicion se cumple
    if ( (numero % 7 === 0) && (numero % 3 === 0) ) {
        response = "El numero es multiplo de 7 y de 3";
    } else {
        response = "El numero no es multiplo de 7 y/o no es multiplo de 3";
    }

    // Imprime variable $response en HTML
    $("#pResponse").html(response);
    
}


