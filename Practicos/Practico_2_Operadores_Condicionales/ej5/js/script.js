$("#btnSubmit").click(accion);


function accion() {
    
    // Determina Variables
    var numero = Number($("#txtValue").val());
    var response;
    
    // Determinar si la condicion se cumple
    if (numero > 10 && numero < 20) {
        response = "El numero es mayor que 10 y menor que 20";
    } else {
        response = "El numero no es mayor que 10 y/o no es menor que 20";
    }

    // Imprime variable $response en HTML
    $("#pResponse").html(response);
    
}


