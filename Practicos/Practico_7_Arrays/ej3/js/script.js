// Declaracion de los productos a la venta
var guitarras = [
	{tipo: 1, nombre: "Clasica", precio: 2000},
	{tipo: 2, nombre: "Electrica", precio: 2500},
	{tipo: 3, nombre: "Electroacustica", precio: 2300}
];

// Agregar contenido del Array Guitarras como Options al slcTipoGuitarra
function popularSelect() {
	for (var i = 0; i<guitarras.length; i++) {
		$("#slcTipoGuitarra").append("<option value='" + guitarras[i].tipo + "'>" + guitarras[i].nombre + "</option>");
	}
}

$(".btn").click(mostrarSeccion);
$("#btnVender").click(venderGuitarras);
ocultarSecciones();
popularSelect();

// Declaracion de Array Ventas Vacio (todavia)
var ventas = [];

// Vender Guitarras! Woo!
function venderGuitarras() {
	// Tomar valores de los inputs HTML
	var _tipo = Number($("#slcTipoGuitarra").val());
	var _cantidad = Number($("#txtCantidad").val());
	
	// Chequear que Cantidad es un Numero
	if (!isNaN(_cantidad)) {
		
		// Almacenar Datos de Nueva Venta
		var _venta = {
			tipoGuitarra: _tipo,
			cantidadGuitarras: _cantidad
		};
		
		// Almacenar nueva venta en el listado de Ventas
		ventas.push(_venta);
		
		// Confirmar que la venta fue valida
		$("#avisos").html("Venta Procesada");
		
	} else {
		// Si cantidad no es un numero, alertar que los datos no son validos
		$("#avisos").html("Datos no Validos");
	}
	
	// Armamos Tabla de Guitarras
	armarTablaGuitarras();
	
}

// Ocultar todas las secciones y mostrar la seccion que corresponde con el boton clickeado
function mostrarSeccion() {
	ocultarSecciones();
	var _idBtn = $(this).attr("id");
	var _seccion = "seccion" + _idBtn.substr(10);
	$("#" + _seccion).show();
}

// Ocultar todas las secciones
function ocultarSecciones() {
	$(".seccion").hide();
}

// Devolver el total de pesos para cada guitarra vendida
function obtenerTotalPesosTipoGuitarra(_tipoGuitarra) {
	// Variables a ser usadas
	var _totalPesos;
	var _precioGuitarra;
	var _cantidadGuitarras = 0;
	
	// Conseguimos el precio para el tipo de guitarra ingresado
	for (var i = 0; i < guitarras.length; i++) {
		if (guitarras[i].tipo == _tipoGuitarra) {
			_precioGuitarra = guitarras[i].precio;
			break;
		}
	}
	
	// Buscamos entre las ventas las que son con esa guitarra en particular, y sacamos la cantidad total
	// vendida de nuestra guitarra
	for (var i = 0; i < ventas.length; i++) {
		if (ventas[i].tipoGuitarra == _tipoGuitarra) {
			_cantidadGuitarras = _cantidadGuitarras + ventas[i].cantidadGuitarras;
		}
	}
	
	// Devolvemos el total de pesos para la guitarra con la cantidad y el precio
	_totalPesos = _precioGuitarra * _cantidadGuitarras;
	return _totalPesos;
}


// Mostrar ventas por guitarra de obtenerTotalPesosTipoGuitarra() en una tabla
function armarTablaGuitarras() {
	$("#tblGuitarras").empty();
	for (var i = 0; i < guitarras.length; i++) {
		$("#tblGuitarras").append("<tr><td>" + guitarras[i].nombre + "</td><td>" + obtenerTotalPesosTipoGuitarra(guitarras[i].tipo) + "</td></tr>");
	}
}



