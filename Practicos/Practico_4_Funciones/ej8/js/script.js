$("#btnIngresar").click(mostrar);

function printHtml(_toPrint) {
    // Imprimir el parametro en HTML
    $("#pTexto").html(_toPrint);
}

function calcularPotencia(_numero, _potencia) {
    var _resultado = _numero;
    for (i = 1; i < _potencia; i++) {
        _resultado = _resultado * _numero;
    }
    return _resultado;
    
}

function mostrar() {
    var numero = Number($("#txtNumero").val());
    var potencia = Number($("#txtPotencia").val());
    
    printHtml(calcularPotencia(numero, potencia));
}


