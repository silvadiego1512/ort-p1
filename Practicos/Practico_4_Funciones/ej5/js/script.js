$("#btnIngresar").click(mostrar);

function printHtml(_toPrint) {
    // Imprimir el parametro en HTML
    $("#pTexto").html(_toPrint);
}

function calcularAreaRectangulo(_ancho, _altura) {
    // Chequear que ambos sean positivos
    if (_ancho < 0 || _altura < 0) {
        // Devolver -1 si no son positivos
        return -1;
    } else {
        // Si ambos son positivos, calcular area y returnearla
        return (_ancho * _altura);
    }
}

function mostrar() {
    var ancho = Number($("#txtAncho").val());
    var altura = Number($("#txtAltura").val());
    
    // Llamar a la funcion de area y mostrarla con la funcion de muestra
    printHtml(calcularAreaRectangulo(ancho, altura));
}


