$("#btnIngresar").click(mostrar);

function printHtml(_toPrint) {
    // Imprimir el parametro en HTML
    $("#pTexto").html(_toPrint);
}

function conversorTemp(_tempF, _destUnit) {
    switch (_destUnit) {
        case "kelvin":
            return ((_tempF + 459.67) / 1.8);
            break;
        case "celsius":
            return ((_tempF - 32) / 1.8);
            break;
        case "rankine":
            return (_tempF + 459.67);
            break;
        case "reaumur":
            return ((_tempF - 32) / 2.25);
            break;
    }
    
}

function mostrar() {
    var temp = Number($("#txtTemperatura").val());
    var unidadDest = $("#txtUnidadMedida").val();
    
    printHtml(conversorTemp(temp, unidadDest));
}


