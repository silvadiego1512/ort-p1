$("#btnIngresar").click(mostrar);

function printHtml(_toPrint) {
    // Imprimir el parametro en HTML
    $("#pTexto").html(_toPrint);
}

function calcularAreaTriangulo(_base, _altura) {
    // Chequear que ambos sean positivos
    if (_base < 0 || _altura < 0) {
        // Devolver -1 si no son positivos
        return -1;
    } else {
        // Si ambos son positivos, calcular area y returnearla
        return ((_base * _altura) / 2);
    }
}

function mostrar() {
    var base = Number($("#txtBase").val());
    var altura = Number($("#txtAltura").val());
    
    // Llamar a la funcion de area y mostrarla con la funcion de muestra
    printHtml(calcularAreaTriangulo(base, altura));
}


