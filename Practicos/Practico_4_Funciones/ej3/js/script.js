$("#btnIngresar").click(mostrar);

function esBisiesto(_year) {
    if (_year % 4 == 0) {
        if (_year % 100 != 0) {
            return true;
        } else if (_year % 400 == 0) {
            return true;
        } else {
            return false
        }
    } else {
        return false;
    }
}


function mostrar() {
    
    var year = Number($("#txtYear").val());
    
    $("#pTexto").html(esBisiesto(year).toString());
    
}


