// 
// OPERADORES LOGICOS
//

var numero;

// Operador AND (representado en JavaScript con &&)
// Para que devuelva TRUE ambas condiciones deben cumplirse
if (numero > 40 && numero < 60) {
	// Devolvera TRUE del 41 al 59
}

// Operador OR (representado en JavaScript con ||)
// Para que devuelva TRUE basta con que una condicion se cumpla
if (numero < 20 || numero > 60) {
	// Devolvera TRUE hasta el 19 y despues del 61
}

// Operador NO (representado en JavaScript con !)
// Invierte el TRUE a FALSE (!TRUE = NOT TRUE)