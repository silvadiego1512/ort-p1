var number1;
var number2;
var result;

$("#btnSubmit").click(calcular);

function calcular()
{
	number1 = Number($("#txtNumber1").val());
	number2 = Number($("#txtNumber2").val());
	
	result = number1 + number2;
	
	$("#pTotal").html("El resultado de sumar " + number1 + " y " + number2 + " es " + result);
}
